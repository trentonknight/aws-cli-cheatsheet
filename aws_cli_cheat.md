# AWS CLI Cheat Sheet
* [IAM latest UserGuide best-practices](https://docs.aws.amazon.com/IAM/latest/UserGuide/best-practices.html)
* [awscli v2](https://awscli.amazonaws.com/v2/documentation/api/latest/index.html)

# Access Keys
* [IAM CreateAccessKey](https://docs.aws.amazon.com/IAM/latest/UserGuide/id_credentials_access-keys.html?icmpid=docs_iam_console#Using_CreateAccessKey)

Before a User can perform any acitions they must be directly added to a policy or a group containing policies which will allow them to run queries or other commands. The below examples include the ARNs required. To view the current users 

### AdministratorAccess
#### Amazon Resource Name (ARN) 

View ARN policies using the following command:

```bash
aws iam get-policy --policy-arn arn:aws:iam::aws:policy/AdministratorAccess
```

```json
arn:aws:iam::aws:policy/AdministratorAccess
```
```json
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": "*",
            "Resource": "*"
        }
    ]
}
```

#### User Info

List all Users

```bash
aws iam list-users
``` 
Get a Users basic info 
```bash
aws iam get-user --user-name DevOpsAlpha
```
Get group information of current cli user:

```bash
aws iam list-groups
```



## EC2

User must be in group which includes the following policy:

### AmazonEC2FullAccess
#### Amazon Resource Name (ARN) 

```json
arn:aws:iam::aws:policy/AmazonEC2FullAccess
```
Provides full access to Amazon EC2 via the AWS Management Console.

```json
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Action": "ec2:*",
            "Effect": "Allow",
            "Resource": "*"
        },
        {
            "Effect": "Allow",
            "Action": "elasticloadbalancing:*",
            "Resource": "*"
        },
        {
            "Effect": "Allow",
            "Action": "cloudwatch:*",
            "Resource": "*"
        },
        {
            "Effect": "Allow",
            "Action": "autoscaling:*",
            "Resource": "*"
        },
        {
            "Effect": "Allow",
            "Action": "iam:CreateServiceLinkedRole",
            "Resource": "*",
            "Condition": {
                "StringEquals": {
                    "iam:AWSServiceName": [
                        "autoscaling.amazonaws.com",
                        "ec2scheduled.amazonaws.com",
                        "elasticloadbalancing.amazonaws.com",
                        "spot.amazonaws.com",
                        "spotfleet.amazonaws.com",
                        "transitgateway.amazonaws.com"
                    ]
                }
            }
        }
    ]
}
```

### EC2 Instances

* [awscli ec2](https://awscli.amazonaws.com/v2/documentation/api/latest/reference/ec2/index.html)

List all instances:

```bash
aws ec2 describe-instances
```

Show only instances with the instance-type value of t2.micro: 

```bash
aws ec2 describe-instances --filters "Name=instance-type,Values=t2.micro" --query "Reservations[].Instances[].InstanceId"
```
Cat instances into a text file.
```bash
aws ec2 describe-instances --filters "Name=instance-type,Values=t2.micro" --query "Reservations[].Instances[].InstanceId" > instances.md
```

#### Stopping Instances 

Stopping an Instance allows you to restart it again later. To full delete an instance see below for Terminating an instance.

```bash
aws ec2 stop-instances --instance-ids i-1234567890abcdef0
```

Verify the instance shutdown and has been deleted:

```bash
aws ec2 describe-instances  --instance-ids i-0912d10faafda925a  --query "Reservations[].Instances[].State.Name"
```

#### Terminating Instances:

```bash
aws ec2 terminate-instances --instance-ids i-0712d10faafda825a 
```
Verify the instance shutdown and has been deleted:

```bash
aws ec2 describe-instances  --instance-ids i-0912d10faafda925a  --query "Reservations[].Instances[].State.Name"
```

