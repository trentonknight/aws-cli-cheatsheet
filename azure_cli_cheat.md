# Azure cli cheat sheet

## Subscription info

```bash
subscriptionID=$(az account show --query id -o tsv)
echo $subscriptionID
```

## Resource groups
* [Manage Azure Resource Groups by using Azure CLI](https://learn.microsoft.com/en-us/azure/azure-resource-manager/management/manage-resource-groups-cli)
### Create a resource group

```bash
az group create --name demoResourceGroup --location eastus
```
### Delete a resource group

```bash
az group delete --name exampleGroup
```

### List resource groups

```bash
az group list
```
By name

```bash
az group show --name exampleGroup
```

## Creating Service Principal

```bash
let "randomIdentifier=$RANDOM*$RANDOM"  
servicePrincipalName="msdocs-sp-$randomIdentifier"
roleName="Contributor"
subscriptionID=$(az account show --query id -o tsv)
# Verify the ID of the active subscription
echo "Using subscription ID $subscriptionID"
resourceGroup="myResourceGroupName"

echo "Creating SP for RBAC with name $servicePrincipalName, with role $roleName and in scopes /subscriptions/$subscriptionID/resourceGroups/$resourceGroup"
az ad sp create-for-rbac --name $servicePrincipalName --role $roleName --scopes /subscriptions/$subscriptionID/resourceGroups/$resourceGroup
```
